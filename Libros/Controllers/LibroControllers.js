'use strict'

var Libro = require('../Modelos/Libro.js');

function guardar(req,res)
{
    let libro= new Libro()
    libro.Nombre = req.body.Nombre
    libro.Autor= req.body.Autor
    libro.Año_publicacion= req.body.Año_publicacion
    libro.Idioma= req.body.Idioma

    libro.save((err, librostore)=>
    {
        if(err) return res.status(401).send(`Error base de datos> ${err}`)
        res.status(200).send({libro:librostore})
    })
 
}

function getLibros(req, res){
    Libro.find({}).sort({'_id':-1}).exec((err, Libro) => 
    {
        if(err) return res.status(500).send({message: 'Error en el servidor'})
            if(Libro){
                return res.status(200).send({
                    Libro
                })
            }else{
                return res.status(404).send({
                    message: 'No hay Libros'
                })
            }
     
    });
}


function getLibro(req, res)
{
    var LibroID = req.params.id

    Libro.findById(LibroID).exec((err, Libro) => {
        if(err) return res.status(500).send({ message: 'Error en el servidor' })

            if(Libro){
                return res.status(200).send({
                    Libro
                });
            }else{
                return res.status(404).send({
                    message: 'No existe el Libro'
                });
            }
        
    });
}

function modificar(req, res)
{
    var LibroID = req.params.id
    var update = req.body
    Libro.findByIdAndUpdate(LibroID, update, {new:true}, (err, LibroUpdate) => {
        if(err) return res.status(500).send({message: 'Error en el servidor'})
        
            if(LibroUpdate){
                return res.status(200).send({
                    Libro: LibroUpdate
                });
            }else{
                return res.status(404).send({
                    message: 'No existe el libro'
                });
            }
        
    });
}


function Borrar(req, res){
    var LibroID = req.params.id
 
        // Buscamos por ID, eliminamos el objeto y devolvemos el objeto borrado en un JSON
    Libro.findByIdAndRemove(LibroID, (err, LibroRemoved) => {
        if(err) return res.status(500).send({ message: 'Error en el servidor' })
         
            if(LibroRemoved){
                return  res.status(200).send({
                    Libro: LibroRemoved
                    
                })
            }else{
                return  res.status(404).send({
                    message: 'No existe el libro'
                })
            }
         
    })
}

module.exports =
{
    guardar,
    getLibro,
    getLibros,
    modificar,
    Borrar
};

