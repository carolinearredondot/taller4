'use strict'

const mongoose =require('mongoose')
const Schema = mongoose.Schema
const LibroSchema = Schema({
    Nombre: String,
    Autor: String,
    Año_publicacion: Number,
    Idioma: {
        type: String,
        enum:['Español','Ingles']
    }

})

module.exports= mongoose.model('Libro',LibroSchema)
