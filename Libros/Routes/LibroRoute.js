'use strict'
var express = require('express');

var LibroControllers = require('../Controllers/LibroControllers');
const Libro = require('../Modelos/Libro');

var api= express.Router();

api.post('/Libro',LibroControllers.guardar);

api.get('/Libro/:id',LibroControllers.getLibro);
api.get('/Libro',LibroControllers.getLibros);
api.put('/Libro/:id',LibroControllers.modificar);
api.delete('/Libro/:id',LibroControllers.Borrar);


module.exports =api;

